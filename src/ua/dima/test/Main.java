package ua.dima.test;

public class Main {

    public static void main(String[] args) {
        Vampiru.get();
    }
}
class Vampiru {
    static void get() {
        outer:
        for (int i = 1000; i <= 9999; i++) {
            //размерность 24 взята из расчета 4!
            int rez[][] = new int[24][4];
            int num[] = new int[4];
            //итератор сдвига масива в право для добавления новой комбинации
            int iterator = 0;
            //отделяем числа
            num[0] = i / 1000;
            num[1] = (i / 100) % 10;
            num[2] = (i / 10) % 10;
            num[3] = i % 10;
            //формируем пары
            for (int j = 0; j < 4; j++) {
                for (int k = 0; k < 4; k++) {
                    if (k == j) {
                        continue;
                    }
                    for (int l = 0; l < 4; l++) {
                        if (l == k || l == j) {
                            continue;
                        }
                        for (int m = 0; m < 4; m++) {
                            if (m == j || m == k || m == l) {
                                continue; }
                            //заполняем масив
                            rez[iterator][0] = num[j];
                            rez[iterator][1] = num[k];
                            rez[iterator][2] = num[l];
                            rez[iterator][3] = num[m];
                            iterator++;
                        }
                    }
                }
            }
            for (int j = 0; j < 24; j++) {
                if ((rez[j][0] * 10 + rez[j][1]) * (rez[j][2] * 10 + rez[j][3]) == i) {
                    System.out.println("Число вампир "+i + "=" + (rez[j][0] * 10 + rez[j][1]) + " * " + (rez[j][2] * 10 + rez[j][3]));
                    continue outer;
                }
            }
        }
    }
}